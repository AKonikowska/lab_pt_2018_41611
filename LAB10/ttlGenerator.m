function [t] = ttlGenerator(CLK,data)
    i=1;
    
    for n=1:length(CLK)             
        
     t(n)=data(i);                   
        
        if(n+1<length(CLK))            
            
            if(CLK(n)==0 && CLK(n+1)==1)
                
                i=i+1;
            
            end
            
        end
    end
end