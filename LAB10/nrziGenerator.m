function [s] = nrziGenerator(~,data)
streamLen = length(data); % length of the stream
positionTime = 0.0001; % position duration
endTime = streamLen - positionTime; % required end time for given bit string
t = 0:positionTime:endTime;
j = 1; % index of the signal vector, s   https://www.mathworks.com/matlabcentral/fileexchange/60391-nrz-i-code-matlab-communication
bit = 1; % current bit
n = 1;
for i = 0:positionTime:endTime %
 if (floor(i)+1 ~= bit) % checks whether in the same bit
 bit = bit + 1;
 f = 0;
 end
 if (data(bit) == 0) % zero voltage for bit 0
     s(j) = n;
 else
     if(~f) % positive voltage for bit 1
         s(j) = n*-1; 
         n = s(j);
         f = 1;
         j = j + 1;
         continue; 
     end
     s(j) = n;
 end
 j = j + 1;
end
end