function[F]=YUV2FDCT(f)
F=zeros(8,8);
for u=1:8
    for v=1:8
         sxy=0;
        for x=1:8
            for y=1:8
                sxy=sxy+(f(x,y)*cos((((2*(x-1))+1)*(u-1)*pi)/16)*cos((((2*(y-1))+1)*(v-1)*pi)/16));
            end
        end
         F(u,v)=sxy*C(u-1)*C(v-1)*(1/4);
    end
end
end