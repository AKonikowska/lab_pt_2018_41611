function [Y, U, V] = rgb2yuv( R, G, B )
Y=floor(0.3*(double(R))+0.59*(double(G))+0.11*(double(B)));
U=floor(0.56*(double(B)-Y)+128);
V=floor(0.71*(double(R)-Y)+128);
end