function[ uY, uU, uV ] = sign2unsign( zY, zU, zV )
idx=find(zY<0);
zY=abs(zY)*2;
zY(idx)=zY(idx)+1;
uY=zY;
idx2=find(zU<0);
zU=abs(zU)*2;
zU(idx2)=zU(idx2)+1;
uU=zU;
idx3=find(zV<0);
zV=abs(zV)*2;
zV(idx3)=zV(idx3)+1;
uV=zV;
end