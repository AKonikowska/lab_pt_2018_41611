%BMP->JPEG

clear all
close all
clc

%macierz kwantyzacji dla wsp�czynnik�w luminancji
Qy = ...
   [ 16  11  10  11  24  40  51  61
     12  12  14  19  26  58  60  55
     14  13  16  24  40  57  69  56
     14  17  22  29  51  87  80  62
     18  22  37  56  68 109 103  77
     24  35  55  64  81 104 113  92
     49  64  78  87 103 121 120 101
     72  92  95  98 112 100 103  99];

%macierz kwantyzacji dla wsp�czynnik�w chrominancji
Quv = ...
   [ 17  18  24  47  99  99  99  99
     18  21  26  66  99  99  99  99
     24  26  56  99  99  99  99  99
     47  66  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99];

%wsp�czynnik kompresji
c=1.0;

% odczyt obrazu
imgBMP = imread('lennacolor24b.bmp');

%wycinek obrazu 8x8 pikseli RGB
excerpt = imgBMP(1:8,1:8,:);
%TU ROZBUDUJ SKRYPT ABY WYCINA� W P�TLI
%AUTOMATYCZNIE KAWA�EK PO KAWA�KU FRAGMENTY 8x8 PIKSELI 
%Z OBRAZU 

%wydobycie sk�adowych koloru .
[R, G, B] = getRGB(excerpt);

%zmiana przestrzeni barw RGB na luminancje i chrominancje . 
[Y, U, V] = rgb2yuv( R, G, B );

%konwersja przedzia��w warto�ci z [0..255] na [-128..127] .
[ Y, U, V ] = mapValue( Y, U, V );

%dyskretna transformacja kosinusowa
[fY] = YUV2FDCT(Y);
[fU] = YUV2FDCT(U);
[fV] = YUV2FDCT(V);

%kwantyzacja wraz z kompresj� uzale�nion� od parametru "c"
[ qY, qU, qV ] = fYUV2qYUV( fY, fU, fV, Qy, Quv, c );
  
%reorganizacja macierzy na wektory
[ zY, zU, zV ] = qYUV2zYUV( qY, qU, qV );

%konwersja zapisu liczb ujemnych w postaci dodatnich nieparzystych
[ uY, uU, uV ] = sign2unsign( zY, zU, zV );

%kodowanie d�ugo�ci serii (RLE)
[rY] = uYUV2RLE(uY);
[rU] = uYUV2RLE(uU);
[rV] = uYUV2RLE(uV);

%utworzenie roboczej struktury na dane jpeg
jpeg = struct('Y', rY, 'U', rU, 'V', rV, 'c', c);

sizeJpeg = length(jpeg.Y) + length(jpeg.U) + length(jpeg.V)+length(jpeg.c);
fprintf('Rozmiar: %dB (%f%% pierwotnego rozmiaru)\n\n', sizeJpeg, 100 * (sizeJpeg /(3*8*8)));