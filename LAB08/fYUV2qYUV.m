function[ qY, qU, qV ] = fYUV2qYUV( fY, fU, fV, Qy, Quv, c )
    Qyp=Qy*c;
    Quvp=Quv*c;
    
    qY=int8(round(fY/Qyp));
    qU=int8(round(fU/Quvp));
    qV=int8(round(fV/Quvp));
    
end