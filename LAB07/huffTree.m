function [ codeBook ] = huffTree( sortedAlphabet, sortedProbabilityDistribution )
%% Source based on elements of : https://www.mathworks.com/examples/matlab-communications/community/20475-huffman-encoding-example
% ************************
% Hufmman Coding Example
%  By Jason Agron
% ************************

% Rotate vector of probability distribution
sortedProbabilityDistribution=sortedProbabilityDistribution';

%% Console Output - Initial Characters and Their Respective Probabilties
% Display character vs. probability

disp('Character Probability:');
for i = 1:length(sortedProbabilityDistribution)
	display(strcat(sortedAlphabet(i),' -->  ',num2str(sortedProbabilityDistribution(i))));
end

%% Initialize The Encoding Array
% A cell array is used to hold the groups of symbols for encoding. The process of huffman encoding combines characters into larger symbols, and a cell is the ideal type of container for managing this type of data in MATLAB. Initialize a cell array (where each cell is a symbol)

for i = 1:length(sortedAlphabet)
	sorted_str{i} = sortedAlphabet(i);
end

% Save initial set of symbols and probabilities for later use
init_str = sorted_str;
init_prob = sortedProbabilityDistribution;

%% Huffman Encoding Process
% Iteratively sort and combine symbols based on their probabilities. Each iteration sorts the available symbols and then takes the two symbols with the "worst" probabilties and combines them into a single symbol for the next iteration. The loop halts when only a single symbol remains.

sorted_prob = sortedProbabilityDistribution;
rear = 1;
while (length(sorted_prob) > 1)
	% Sort probs
	[sorted_prob,indeces] = sort(sorted_prob,'ascend');
	% Sort string based on indeces
	sorted_str = sorted_str(indeces);

	% Create new symbol
	new_node = strcat(sorted_str(1),sorted_str(2));
	new_prob = sum(sorted_prob(1:2));

 
    
	% Dequeue used symbols from "old" queue
	sorted_str =  sorted_str(3:length(sorted_str));
	sorted_prob = sorted_prob(3:length(sorted_prob));

    
	% Add new symbol back to "old" queue
    
	sorted_str = [sorted_str, new_node];
	sorted_prob = [sorted_prob, new_prob];

	% Add new symbol to "new" queue
	newq_str(rear) = new_node;
	newq_prob(rear) = new_prob;
	rear = rear + 1;
end

%% Form Huffman Tree Data
% Get all elements for the tree (symbols and probabilities) by concatenating the original set of symbols with the new combined symbols and probabilities

tree = [newq_str,init_str];
tree_prob = [newq_prob, init_prob];

% Sort all tree elements
[sorted_tree_prob,indeces] = sort(tree_prob,'descend');
sorted_tree = tree(indeces);

%% Calculate Tree Parameters
% Calculate parent relationships for all tree elements. This will allow the treeplot command to realize the correct tree structure.

parent(1) = 0;
for i = 2:length(sorted_tree)
	% Extract my symbol
	me = sorted_tree{i};

	% Find my parent's symbol (search until shortest match is found)
    count = 1;
	parent_maybe = sorted_tree{i-count};
	diff = strfind(parent_maybe,me);
	while (isempty(diff))
		count = count + 1;
        parent_maybe = sorted_tree{i-count};
		diff = strfind(parent_maybe,me);
	end
	parent(i) = i - count;
end

figure
treeplot(parent);
title(strcat('Huffman Coding Tree - "',sortedAlphabet,'"'));

%% Console Output - Tree Symbols and Their Probabilities
% Print out tree in text form
display(sorted_tree)
display(sorted_tree_prob)

%% Tree Parameter Extraction
% Extract binary tree parameters. These parameters include the (x,y) coordinates of each node so that we can accurately place label on the tree.

[xs,ys,h,s] = treelayout(parent);

%% Label Tree Nodes
% Place labels on each tree node

text(xs,ys,sorted_tree);

%% Label Tree Edges
% Put weights on the tree. The slope of each edge indicates whether it is a left-branch or a right-branch. Left-branches have positive slope and are labelled with a '1', while right-branches have negative slope and are labelled with a '0'. The labels for the weights are placed at the midpoint of each edge. Calculate mid-points for each node

for i = 2:length(sorted_tree)
	% Get my coordinate
	my_x = xs(i);
	my_y = ys(i);

	% Get parent coordinate
	parent_x = xs(parent(i));
	parent_y = ys(parent(i));

	% Calculate weight coordinate (midpoint)
	mid_x = (my_x + parent_x)/2;
	mid_y = (my_y + parent_y)/2;

	% Calculate weight (positive slope = 1, negative = 0)
	slope  = (parent_y - my_y)/(parent_x - my_x);
	if (slope > 0)
		weight(i) = 1;
	else
		weight(i) = 0;
	end
	text(mid_x,mid_y,num2str(weight(i)));
end

%% Huffman Codebook Calculation
% A huffman codebook can be calculated by traversing the huffman tree while recording the weights encountered while travelling to each node. Calculate codebook

for i = 1:length(sorted_tree)
	% Initialize code
	code{i} = '';

	% Loop until root is found
	index = i;
	p = parent(index);
	while(p ~= 0)
		% Turn weight into code symbol
		w = num2str(weight(index));

		% Concatenate code symbol
		code{i} = strcat(w,code{i});

		% Continue towards root
		index = parent(index);
		p = parent(index);
	end
end

%% Display raw code book with prefixes
codeBook = [sorted_tree', code']

%% Rewind code book and clear prefixes
rear=1;
codeBookNonPrefix=cell(1,2);
for i = 1:length(sorted_tree)
    if length(codeBook{i,1})==1
        codeBookNonPrefix{rear,1}=codeBook{i,1};
        codeBookNonPrefix{rear,2}=codeBook{i,2};
        rear=rear+1;
    end
end
codeBook = codeBookNonPrefix;

end