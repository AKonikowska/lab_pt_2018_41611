% Wczytanie pliku
[lr,Fs]=audioread('everything-is-fine.wav');

%Wyb�r tylko jednego kana�u
l=lr(:,1);

% Obliczenie Fs-punktowej transformaty
L=fft(l,Fs);

% Odtworzenie sygna�u l, bez zerowania sk��dowych
sound(l,Fs)
subplot(4,1,1);
plot(l);
title('odtwarzanie sygnalu l, bez zerowania skladowych')
ylabel('l')
xlabel('czas')
grid on;

% Odczekanie 4s
pause(4);

% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.93;

% Obracanie widma
L=fftshift(L);
subplot(4,1,2);
plot(abs(L));
title('Obracanie widma')
ylabel('L')
xlabel('czestotliwosc')
grid on;

% Obliczenie progu odci�cia i wyzerowanie kranc�w widma
r=floor((Fs/2)*q);
L(1:r)=0;
L(Fs-r:Fs)=0;

% Obracanie widma do pierwotnej kolejno�ci
L=fftshift(L);
subplot(4,1,3);
plot(abs(L));
title('Obracanie widma do pierwotnej kolejno�ci')
ylabel('L')
xlabel('czestotliwosc')
grid on;

% Odwrotna transformata
l=real(ifft(L));


% Odtworzenie sygna�u l z wyzerowan� cz�ci� widma
sound(l,Fs);
subplot(4,1,4);
plot(l);
title('Odtwarzanie sygnalu l z wyzerowana czescia widma')
ylabel('l')
xlabel('czas')
grid on;
