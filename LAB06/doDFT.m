function[a]=doDFT(A)

N=max(size(A));
a=zeros(N,1);

for k=1:N
for n=1:N
    a(k)=a(k)+(A(n)*(cos(2*pi*(k)/N)+i*sin((2*pi*(k)*n)/N)));
end
a(k)=(1/N)*a(k);
end
