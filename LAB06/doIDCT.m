function [l] = doIDCT(L)
T=max(size(L));
l=zeros(T,1);
for m=1:T-1
    l(1)=l(1)+L(m);
end
l(1)=l(1)/sqrt(T);  %G(0)=1/sqrt(N) suma g(m)
for k=2:T
    for m=1:T-1
    l(k)=l(m)+l(k)+(L(m)*cos(pi*k*(2*m+1))/(2*T));
    end
l(k)=l(k)*sqrt(2/T);
end
end

