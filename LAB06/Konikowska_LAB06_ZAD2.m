close all
clear all
clc

% Odczyt pliku audio
[lr,Fs]=audioread('everything-is-fine.wav');
% Wyb�r jednego kana�u
l=lr(:,1);

% Okre�lenie wybranego fragmentu czasu (zmniejszenie d�ugo�ci pr�bki)
t0=floor(Fs*0.1);
t1=floor(t0*2);
l=lr(t0:t1,1);

figure
subplot(3,1,1),plot(l)

% Dyskretna transformata kosinusowa
L=doDCT(l);

subplot(3,1,2),plot(L)

% Dyskretna odwrotna transformata kosinusowa
l=doIDCT(L);
subplot(3,1,3),plot(l)

xy = imread('lennagrey.png');
xy = rgb2gray(xy);
figure,imshow(xy)

% Dwuwymiarowa dyskretna transformacja kosinusowa
XY = dct2(xy);
figure,imshow(abs(XY))
figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar

% Dwuwymiarowa dyskretna odwrotna transformacja kosinusowa
xy_ = idct2(XY);

figure,imshowpair(xy,xy_,'montage')
title('Oryginalny obraz (lewy), przetworzony (prawy)');