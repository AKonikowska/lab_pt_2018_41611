close all
clear all
clc

[lr,fs]=audioread('everything-is-fine.wav');

fs=44100;
t=0:(1/fs):0.2;
offset=length(t);
lr=lr(1:offset);

figure(1);
plot(t,lr);
ylabel('lr')
title('widmo sygna�u')
xlabel('czestotliwosc')
grid on;

dec=10*log10(lr);
figure(2);
plot(t,dec);
title('widmo w skali decybelowej')
ylabel('lr')
xlabel('czestotliwosc')
grid on;