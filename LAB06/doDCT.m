function [L] = doDCT(l)
T=max(size(l));
L=zeros(T,1);
for m=1:T-1
    L(1)=L(1)+l(m);
end
L(1)=L(1)/sqrt(T);  %G(0)=1/sqrt(N) suma g(m)
for k=1:T %lub 2:T
    for m=1:T-1
    L(k)=L(k)+(l(m)*cos((pi*k*(2*m+1))/(2*T)));
    end
L(k)=L(k)*sqrt(2/T);
end
end


