% Badanie sygna�u 2-D
% Odczyt pliku graficznego
xy = imread('lennagrey.png');
% Przekszta�cenie formatu RGB na 2-D odcienie szaro�ci
xy = rgb2gray(xy);
% Rysowanie
figure,imshow(xy)

% Obliczenie dwuwymiarowego fft
XY=fft2(xy);

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar 
title('Obliczanie dwuwymiarowego fft');
ylabel('dlugosc'), xlabel('szerokosc'), zlabel('amplituda');

% Obr�t w symetrii transformaty
XY=fftshift(XY);

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar
title('Obr�t w symetrii transformaty');
ylabel('dlugosc'), xlabel('szerokosc'), zlabel('amplituda');

% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.37;
% Zerowanie pewnego pasma dolnych cz�stotliwo�ci
XY(256-floor(128*q):256+floor(128*q),256-floor(128*q):256+floor(128*q))=0;

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar
title('Zerowanie pasma dolnych cz�stotliwo�ci');
ylabel('dlugosc'), xlabel('szerokosc'), zlabel('amplituda');

% Obr�t w symetrii transformaty do pierwotnej postaci
XY=fftshift(XY);

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar
title('Obr�t w symetrii');
ylabel('dlugosc'), xlabel('szerokosc'), zlabel('amplituda');

% Odwrotna transformata 2-D
xy=real(ifft2(XY));

figure,imshow(xy);
title('Odwrotna transformata 2-D');
ylabel('dlugosc'), xlabel('szerokosc'), zlabel('amplituda');