close all
clc

% opcjonalnie do testów
% A = 1;
% f = 4;
% fs = 100;
% T = 1;
% fi = pi;
% t = 0:1/fs:T-(1/fs);
% x = A*sin(2*pi*f*t+fi);
load x
fs = 1024;
x = x.*hanning(fs,'symmetric');

X = doDFT(x); % DFT z x
m = abs(X); % Widmo amplitudowe

 ind=find(m<0.001); %opcjonalne odszumianie
 X(ind)=0;

p = unwrap(angle(X)); %Widmo fazaowe

f = 0:length(X)-1; % Wektor częstotliwości i czasu do wykresów
t = 0:1/fs:1-(1/fs);

subplot(4,1,1);
plot(t,x);
title('x'), ylabel('A'), xlabel('t'), grid on;

subplot(4,1,2)
plot(f,m)
title('|FFT(x)|'), ylabel('Magnitude'), xlabel('f'), grid on;

subplot(4,1,3)
plot(f,(p*180/pi)+90)
title(''), ylabel('Phase'), xlabel('f'), grid on;


subplot(4,1,4)
x2 = doIDFT(X); %Odwrotna transformacja Fouriera
plot(t,x2);
title('IFFT(x)'), ylabel('A'), xlabel('t'), grid on;