function[X]=doDFT(x)

N=max(size(x));
X=zeros(N,1);

for k=1:N
for n=1:N
    X(k)=X(k)+(x(n)*(cos(2*pi*(-k)*n/N)+1i*sin((2*pi*(-k)*n)/N)));
end
X(k)=(1/N)*X(k);
end
end

%X(n) - warto�� pr�bki sygna�u
%N - liczba pr�bek
%i - wsp�czynnik skr�tu (i sin x)
%k - numer harmonicznej
