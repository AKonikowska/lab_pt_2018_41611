close all
clear all
clc

 Amp=1.0;
 f=10;
 T=1;
 fs=1024;
 t=0:(1/fs):T-(1/fs);
 fi=0;
 a=Amp*sin(2*pi*f*t+fi);
 figure;
 plot(a);


A=DFT(a);
figure;
plot(abs(A));

figure;
plot(real(DFT(A)));