close all
clear all
clc

 Amp=1.0;
 f=10;
 T=1;
 fs=1024;
 t=0:(1/fs):T-(1/fs);
 fi=0;
 a=Amp*sin(2*pi*f*t+fi);
 %figure;
% plot(t,a);
% A=fft(a);
% figure;
% plot(abs(A));

N=max(size(a));
A=zeros(N,1);

for k=1:N
for n=1:N
    A(k)=A(k)+(a(n)*(cos((2*pi*(-k)*n)/N)+i*sin((2*pi*(-k)*n)/N)));
end
end

figure;
plot(abs(A))
