function[x2]=doIDFT(X)

N=max(size(X));
x2=zeros(N,1);

for k=1:N
for n=1:N
    x2(k)=x2(k)+(X(n)*(cos((2*pi*(k)*n)/N)+1i*sin((2*pi*(k)*n)/N)));
end
x2(k)=(1/N)*x2(k);
end
end

%x(n) - warto�� pr�bki sygna�u
%N - liczba pr�bek
%i - wsp�czynnik skr�tu
%k - numer harmonicznej