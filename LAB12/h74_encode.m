function [encode_data] = h74_encode(data) %h
G=[1 1 0 1
    1 0 1 1
    1 0 0 0
    0 1 1 1
    0 1 0 0
    0 0 1 0
    0 0 0 1];
encode_data=G*data;
encode_data=mod(encode_data,2);
end