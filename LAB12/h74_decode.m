function [i,idx] = h74_decode(i) %h
H=[1 0 1 0 1 0 1
    0 1 1 0 0 1 1
    0 0 0 1 1 1 1];
idx=H*i;
idx=mod(idx,2);
idx=idx(1)*1+idx(2)*2+idx(3)*4;
if idx>0
    i=i([3 5 6 7]);
else
    i=i([3 5 6 7]);
    i=~i;
end
end