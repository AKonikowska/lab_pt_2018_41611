close all
clear all
clc

%0 dane wejsciowe
msg='ALINAKONIKOWSKA';
alphabet=['A', 'L', 'I', 'N', 'K', 'O', 'W', 'S'];

[ sortedAlphabet, sortedProbabilityDistribution ] = getProb( msg, alphabet );

[ codeBook ] = huffTree( sortedAlphabet, sortedProbabilityDistribution )

%wygenerowanie danych testowych (wektor pionowy)
disp('dane wej�ciowe');
data=[1 0 1 0]'
%1 1 1 1, 1 0 1 0, 1 1 1 0, 1 1 1 1, 0 0 1 0, 0 1 0 1, 0 1 1 0, 0 1 0 0, 1 1 0 0, 0 1 0 0, 0 1 1 1

%zakodowanie kodem korekcyjnym Hamminga(7,4)
disp('dane zakodowane');
encode_data=h74_encode(data)

%celowe uszkodzenie 3-ego lub losowego bitu
disp('dane uszkodzone');
encode_data=spoilBit(3,encode_data)

%korekcja i odkodowanie danych
disp('dane zrekonstruowane i wskazanie kt�ry indeks by� naprawiany');
[decode_data,idx]=h74_decode(encode_data)

%por�wnanie danych odkodowanych z wej�ciowymi
disp('por�wnanie danych zrekonstruowanych i wej�ciowych');
[decode_data data]