function [encode_data]=spoilBit(idx,encode_data)
if idx==0
    idx=randi([1 7],1,1); %ile liczb losowych, ile wierszy
    encode_data(idx)=~encode_data(idx);
else
 encode_data(idx)=~encode_data(idx);
end
end